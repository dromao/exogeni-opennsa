#!/bin/bash 

# Create a reservation for a NSI circuit
# Parameters are: 
# From nsi.site.properties: NSI_HOSTKEY, NSI_HOSTCERT, NSI_CERTDIR, NSI_VERIFY [Optional], NSI_SERVICE, NSI_PROVIDER, NSI_REQUESTER, NSI_USER, NSI_D_START_TAG, NSI_D_END_TAG, NSI_DEBUG [Optional].
# From controller: NSI_L2_SRC, NSI_L2_DST, NSI_BW [Optional], NSI_DURATION, NSI_START_TIME, NSI_END_TIME, NSI_START_TAG, NSI_END_TAG.

# Note:
# If NSI_VERIFY is not set, the certificate will be verified (default = true)
# If NSI_DEBUG is not set, no additional output will be shown (default = false)
# If NSI_BW is not set, it will be 100 Mbps
# Either NSI_START_TIME and NSI_END_TIME, or NSI_DURATION, must be set

# prep

if [ -z ${NSI_BW} ]; then
	# set default bandwidth 100Mbps
	BW=100
else
	# if coming from orca, divide by 10^6 (ORCA uses bps, OpenNSA Mbps)
	BW=$(($NSI_BW/1000000))
	# if less than 100Mbps, set to 100Mbps
	if [ $((${BW} < 100)) = "1" ]; then
		BW=100
	fi
fi


if [ -z ${NSI_START_TIME} ] || [ -z ${NSI_END_TIME} ]; then
	if [ ${NSI_DURATION} ]; then
	    # Tweak this value. NTP helps here as the time needs to be in sync with the OpenNSA server.
	    NSI_START_TIME=10
		NSI_END_TIME=$((NSI_DURATION + NSI_START_TIME))
	else
		echo "start-nsi.sh: NSI_DURATION, NSI_START_TIME and NSI_END_TIME, not set" >&2 
		echo "start-nsi.sh: Either NSI_DURATION or NSI_START_TIME and NSI_END_TIME must be set" >&2 
	fi
fi

# TLS?
if [ $NSI_TLS == "true" ]; then
	TLS="-x" # enable TLS
fi

# Verify certificate?
if [ $NSI_VERIFY == "false" ]; then
	VERIFY="-z" # will skip the verification
fi

# Detailed output?
if [ $NSI_DEBUG == "true" ]; then
	VERBOSE="-v" # verbose output
fi

# send stdout there
OUTFILE="/tmp/nsi.tmp"
trap 'rm -f $OUTFILE' EXIT


let NSI_L2_TAG=NSI_START_TAG-1

while [ -z $CONNECTION_ID ]; do

	if [ "$NSI_L2_TAG" -lt "$NSI_END_TAG" ]; then
    	let NSI_L2_TAG=NSI_L2_TAG+1
    else
    	echo "There are no vlan tags available!" >&2
    	exit 1
    fi
    
	if [ ! -z ${VERBOSE} ]; then
    	echo "command: python2.7 /usr/bin/onsa reserveprovision -r ${NSI_REQUESTER} -p ${NSI_PROVIDER} -s ${NSI_L2_SRC}#${NSI_L2_TAG} -d ${NSI_L2_DST}#${NSI_L2_TAG} -j user=${NSI_USER} -u ${NSI_SERVICE} -a +${NSI_START_TIME} -e +${NSI_END_TIME} -l ${NSI_HOSTCERT} -k ${NSI_HOSTKEY} -i ${NSI_CERTDIR} -b ${NSI_BW} ${TLS} ${VERIFY} ${VERBOSE} > ${OUTFILE}" >&2 
	fi

	python2.7 /usr/bin/onsa reserveprovision -r $NSI_REQUESTER -p $NSI_PROVIDER -s $NSI_L2_SRC#$NSI_L2_TAG -d $NSI_L2_DST#$NSI_L2_TAG -j user=$NSI_USER -u $NSI_SERVICE -a +$NSI_START_TIME -e +$NSI_END_TIME -l $NSI_HOSTCERT -k $NSI_HOSTKEY -i $NSI_CERTDIR -b $NSI_BW $TLS $VERIFY $VERBOSE > $OUTFILE

	CONNECTION_ID=`grep provisioned ${OUTFILE} | cut -d ' ' -f 2`
	NO_AVAL=`grep "not available in specified time span" ${OUTFILE}`

	# Check if the reservation failed because the VLAN is in use
	if [ -z "$NO_AVAL" ] && [ -z "$CONNECTION_ID" ]; then
		echo "Reservation failed for an unknown reason." >&2
		cat ${OUTFILE} >&2
    	exit 1
	fi
done

# This should be the only content of stdout
echo $CONNECTION_ID" "$NSI_L2_TAG

exit 0